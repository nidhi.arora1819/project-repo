package TestNG;

import org.testng.annotations.Test;

public class DemoOne {
	
	@Test
	public void firstTestCase() {
		System.out.println("I am the first test case from demo one class");
		
	}
	
	@Test
	public void secondTestCase() {
		System.out.println("I am the second test case from demo one class");	
	}
	
	

}
