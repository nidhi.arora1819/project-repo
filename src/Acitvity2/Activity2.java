package Acitvity2;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Activity2 {

	public static void main(String[] args) {
		WebDriver driver = new FirefoxDriver ();
		driver.get("https://www.training-support.net");
		String title = driver.getTitle();
		System.out.println("The title of Page is: " + title);
		driver.findElement(By.tagName("a")).click();
		System.out.println("The title of New Page is: " + driver.getTitle());
		driver.close();	
		
	}

}
