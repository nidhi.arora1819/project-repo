package AlchemyJob;


import java.io.FileReader;

import org.testng.Reporter;
import java.io.IOException;
import java.time.Duration;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvException;


public class AlchemyJobAdmin {
	
	WebDriver driver;
	WebDriverWait wait;
	
	@BeforeTest(alwaysRun = true)
	public void BeforeTest() {
		driver = new FirefoxDriver();
		wait = new WebDriverWait(driver, 10);
		
		// Open Browser
		driver. get("https://alchemy.hguy.co/jobs/wp-admin");
	
	}
	
	// 8. Visit the site�s backend and login
	
				@Test(priority=8)
				public void adminLogin() {
					
						driver.findElement(By.id("user_login")).sendKeys("root");
						driver.findElement(By.id("user_pass")).sendKeys("pa$$w0rd");
						driver.findElement(By.id("wp-submit")).click();
						driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
						String loginID = driver.findElement(By.className("display-name")).getText();
						Reporter.log("8. Logged in user is: " + loginID);
						Assert.assertEquals(loginID, "root");
									
						
					}
			// 9. Create a job listing using the backend
				
			@Test(priority=9)
				public void jobListing() throws InterruptedException {
				
				driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
				
				WebElement joblist = driver.findElement(By.xpath("/html/body/div[1]/div[1]/div[2]/ul/li[7]/a/div[3]"));
				Actions builder = new Actions(driver);
				builder.moveToElement(joblist).pause(Duration.ofSeconds(1)).build().perform();
				
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#menu-posts-job_listing > ul:nth-child(2) > li:nth-child(3) > a:nth-child(1) 	")));
				driver.findElement(By.linkText("Add New")).click();
				
				driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
				driver.findElement(By.xpath("//*[@id=\"post-title-0\"]")).sendKeys("HRP");
				driver.findElement(By.id("_job_location")).sendKeys("Anywhere");
				driver.findElement(By.id("_application")).sendKeys("abhiram@cklabs.com");
				driver.findElement(By.id("_company_name")).sendKeys("IBM");
				driver.findElement(By.id("_company_website")).sendKeys("www.ibm.com");
				driver.findElement(By.id("_company_tagline")).sendKeys("International Business Markets");
				driver.findElement(By.id("_job_expires")).sendKeys("April 28, 2020");
				
				driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div/div[1]/div[2]/button[2]")).click();
				
				driver.findElement(By.cssSelector(".editor-post-publish-button")).click();
				//verify new job post
				
				driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
				
				driver.findElement(By.cssSelector("a.wp-has-current-submenu > div:nth-child(3)")).click();
							
				driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
				driver.findElement(By.cssSelector("#post-search-input")).sendKeys("HRP");
				
				driver.findElement(By.id("search-submit")).click();
				String firstSuggestion2 = driver.findElement(By.cssSelector(".job_title")).getText();
				Assert.assertEquals(firstSuggestion2, "HRP");
				Reporter.log("9. New Job posted successfully using site�s backend");
				
				
			}
			
			// 10. Visit the site�s backend and create a new user
			
			@Test(priority=10)
			public void newUser() throws InterruptedException {
				// Return to home page

				driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
				
				WebElement home =driver.findElement(By.xpath("/html/body/div[1]/div[1]/div[2]/ul/li[1]/a/div[3]"));
				Actions builder11 = new Actions(driver);
				builder11.moveToElement(home).pause(Duration.ofSeconds(2)).build().perform();
				
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#menu-dashboard > ul:nth-child(2) > li:nth-child(2) > a:nth-child(1)")));
				driver.findElement(By.linkText("Home")).click();	
				
				
				driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
				WebElement users = driver.findElement(By.cssSelector("a.menu-icon-users > div:nth-child(3)"));
				Actions builder1 = new Actions(driver);
				builder1.moveToElement(users).pause(Duration.ofSeconds(1)).build().perform();
				
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#menu-users > ul:nth-child(2) > li:nth-child(3) > a:nth-child(1)")));
				driver.findElement(By.linkText("Add New")).click();
				
				driver.findElement(By.id("user_login")).sendKeys("Human Resource Manager");
				driver.findElement(By.id("email")).sendKeys("email222ibm@gmail.com");
				driver.findElement(By.id("first_name")).sendKeys("Testing");
				driver.findElement(By.id("last_name")).sendKeys("Users");
				driver.findElement(By.id("url")).sendKeys("www.ibm.com");
				
				driver.findElement(By.cssSelector(".wp-generate-pw")).click();
				
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("pass1-text")));
				
				driver.findElement(By.id("pass1-text")).clear();
				driver.findElement(By.id("pass1-text")).sendKeys("Pa$$w0rd1234567809");
				driver.findElement(By.className("pw-checkbox")).click();
				
				
				
				Select dropdown = new Select(driver.findElement(By.id("role")));
				dropdown.selectByVisibleText("Employer");
				
				driver.findElement(By.id("createusersub")).click();
				
				//Verify if new user is created
				
				
									
				driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
				driver.findElement(By.id("user-search-input")).sendKeys("Human Resource Manager");
				
				driver.findElement(By.id("search-submit")).click();
				String firstSuggestion2 = driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/form/table/tbody/tr/td[1]/strong/a")).getText();
				Assert.assertEquals(firstSuggestion2, "Human Resource Manager");
				Reporter.log("10. New User Created successfully using site�s backend");
				driver.findElement(By.cssSelector("a.wp-has-current-submenu > div:nth-child(3)")).click();
				
				
			}
			
			// 12. Creating a user using an external file
			
			@Test (priority= 12)
			public void add_user_csv() throws IOException, CsvException, InterruptedException {
				
				
				driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
				
				WebElement users = driver.findElement(By.cssSelector("a.menu-icon-users > div:nth-child(3)"));
				Actions builder1 = new Actions(driver);
				builder1.moveToElement(users).pause(Duration.ofSeconds(1)).build().perform();
				
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#menu-users > ul:nth-child(2) > li:nth-child(3) > a:nth-child(1)")));
				driver.findElement(By.linkText("Add New")).click();
				
				//Load CSV file
		        CSVReader reader = new CSVReader(new FileReader("src/Resources/Add_user.csv"));

		        //Load content into list
		        List<String[]> list = reader.readAll();
		        System.out.println("Total number of rows are: " + list.size());

		        //Create Iterator reference
		        Iterator<String[]> itr = list.iterator();

		        //Iterate all values
		        while(itr.hasNext()) {
		            String[] str = itr.next();
		           
					String login = str[0];
					String email = str[1];
					String fname=  str[2];
					String lname=  str[3];
					String url=  str[4];
					String password = str[5];
					
					driver.findElement(By.id("user_login")).sendKeys(login);
					driver.findElement(By.id("email")).sendKeys(email);
					driver.findElement(By.id("first_name")).sendKeys(fname);
					driver.findElement(By.id("last_name")).sendKeys(lname);
					driver.findElement(By.id("url")).sendKeys(url);
					
					driver.findElement(By.cssSelector(".wp-generate-pw")).click();
					
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("pass1-text")));
					
					driver.findElement(By.id("pass1-text")).clear();
					driver.findElement(By.id("pass1-text")).sendKeys(password);
					driver.findElement(By.className("pw-checkbox")).click();
					
						
					driver.findElement(By.id("createusersub")).click();
					
					//Verify user is created
					Thread.sleep(5000);
					driver.findElement(By.cssSelector("#user-search-input")).sendKeys(login);
					driver.findElement(By.cssSelector("#search-submit")).click();
				    String username= driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/form/table/tbody/tr/td[1]/strong/a")).getText();
				    Assert.assertEquals(username,login);
				    Reporter.log("12. User created successfully using csv file and username is : " + username);
				    driver.findElement(By.cssSelector("a.wp-has-current-submenu > div:nth-child(3)")).click();
		        }reader.close();
		        
			
				
			}
			
			
		@DataProvider(name = "userCreation")
		public static Object[][] credentials() {
		    return new Object[][] { { "Newuser1001", "users110000@gmail.com", "New1", "users", "www.ibm.com", "pa$$$w0rd@11233488" }};
		}

		// Here we are calling the Data Provider object with its name
		@Test(dataProvider = "userCreation", priority = 14)
		public void userCreate(String login, String email, String fname, String lname, String url, String password ) throws InterruptedException {
			
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			
			
			WebElement users = driver.findElement(By.cssSelector("a.menu-icon-users > div:nth-child(3)"));
			Actions builder1 = new Actions(driver);
			builder1.moveToElement(users).pause(Duration.ofSeconds(1)).build().perform();
			
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#menu-users > ul:nth-child(2) > li:nth-child(3) > a:nth-child(1)")));
			driver.findElement(By.linkText("Add New")).click();
			
			driver.findElement(By.id("user_login")).sendKeys(login);
			driver.findElement(By.id("email")).sendKeys(email);
			driver.findElement(By.id("first_name")).sendKeys(fname);
			driver.findElement(By.id("last_name")).sendKeys(lname);
			driver.findElement(By.id("url")).sendKeys(url);
			
			driver.findElement(By.cssSelector(".wp-generate-pw")).click();
			
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("pass1-text")));
			
			driver.findElement(By.id("pass1-text")).clear();
			driver.findElement(By.id("pass1-text")).sendKeys(password);
			driver.findElement(By.className("pw-checkbox")).click();
			
				
			driver.findElement(By.id("createusersub")).click();
			
			//Verify user is created
			Thread.sleep(5000);
			driver.findElement(By.cssSelector("#user-search-input")).sendKeys(login);
			driver.findElement(By.cssSelector("#search-submit")).click();
		    String username= driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/form/table/tbody/tr/td[1]/strong/a")).getText();
		    Assert.assertEquals(username,login);
		    Reporter.log("14. User created successfully using Data provider annotation : " + username);
		    Reporter.log("15. End of Project");
			
		}
		
			
		
		@AfterTest(alwaysRun = true)
	    public void afterClass() {
	        //Close browser
	        driver.close();
	    }

	
	

}
