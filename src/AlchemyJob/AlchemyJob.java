package AlchemyJob;

import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvException;

public class AlchemyJob {
	WebDriver driver;
	WebDriverWait wait;
	
	@BeforeTest(alwaysRun = true)
	public void BeforeTest() {
		driver = new FirefoxDriver();
		wait = new WebDriverWait(driver, 10);
		
		// Open Browser
		driver.get("https://alchemy.hguy.co/jobs/");
	
	}
	
	 // 1. Verify the website title
	@Test(priority=1)
	public void websiteTitle() {
		String title = driver.getTitle();
		Reporter.log("1. Title of Website is: " + title);
		Assert.assertEquals(title , "Alchemy Jobs � Job Board Application");
		
	}
	
	// 2. Verify the website heading
	@Test(priority=2)
	public void websiteHeading() {
		WebElement header = driver.findElement(By.xpath("/html/body/div/div/div/div/main/article/header/h1"));
		Reporter.log("2. Heading of website is: " + header.getText());
		Assert.assertEquals(header.getText() , "Welcome to Alchemy Jobs");
		
	}
	
	// 3. Get the url of the header image
	@Test (priority=3)
	public void headerImage() {
		WebElement headerImage = driver.findElement(By.cssSelector(".attachment-large"));
		String url = headerImage.getAttribute("src");
		Reporter.log("3. Header image URL is : " + url);
		
	}
	
	// 4. Verify the website�s second heading
	
	@Test(priority=4)
	public void secondHeading() {
		String header2 = driver.findElement(By.tagName("h2")).getText();
		Reporter.log("4. Second Heading of website is: " + header2);
		Assert.assertEquals(header2 , "Quia quis non");
	}
	
	// 5. Navigate to another page : �Jobs� page on the site.
	@Test(priority=5)
	public void jobsPage() {
		driver.findElement(By.id("menu-item-24")).click();
		String jobtitle = driver.findElement(By.tagName("h1")).getText();
		Reporter.log("5. Title of Jobs page is: " + jobtitle);
		Assert.assertEquals(jobtitle, "Jobs");
		
	}
	
	// 6. Search for a job and apply for it
			@Test(priority=6)
			public void jobApply() {
				driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
				driver.findElement(By.id("search_keywords")).sendKeys("Senior Tester");
				driver.findElement(By.xpath("/html/body/div/div/div/div/main/article/div/div/form/div[1]/div[4]/input")).click();
				WebElement firstSuggestion = driver.findElement(By.cssSelector(".post-210 > a:nth-child(1) > div:nth-child(2) > h3:nth-child(1)"));
				//Wait for the button to be clicked
				wait.until(ExpectedConditions.elementToBeClickable(firstSuggestion));
				firstSuggestion.click();
				//Wait for results to load
				wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.id("post-210")));
				WebElement applyButton = driver.findElement(By.xpath("/html/body/div/div/div/div/main/article/div/div/div/div[3]/input"));
				//Wait for the button to be clicked
				wait.until(ExpectedConditions.elementToBeClickable(applyButton));
				applyButton.click();
				String email = driver.findElement(By.className("job_application_email")).getText();
				Reporter.log("6. Email id to apply for job is: " + email);
			}
	
	// 7. Create a new job listing
			@Test(priority=7)
			public void newJobListing() {
				driver.findElement(By.id("menu-item-26")).click();
				driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
				driver.findElement(By.id("create_account_email")).sendKeys("email12ibm@ibm.com");
				driver.findElement(By.id("job_title")).sendKeys("Sr. Support Engineer");
				driver.findElement(By.id("job_location")).sendKeys("Anywhere");
				
				 Select dropdown = new Select(driver.findElement(By.id("job_type")));

			    //Select second option by visible text
			    dropdown.selectByVisibleText("Full Time");
			    
			    driver.switchTo().frame(0);
			    				
			    driver.findElement(By.cssSelector("#tinymce")).sendKeys("this is for testing purpose only");
				
			    driver.switchTo().parentFrame();
			    
			    driver.findElement(By.id("application")).sendKeys("abhiram@cklabs.com");
				driver.findElement(By.id("company_name")).sendKeys("AlchemyIBM");
				driver.findElement(By.id("company_website")).sendKeys("http://www.jobs.ibm.com");
				driver.findElement(By.id("company_tagline")).sendKeys("THINK");
				driver.findElement(By.id("company_video")).sendKeys("http://shorturl.at/fDIW5");
				driver.findElement(By.id("company_twitter")).sendKeys("@Test_IBM");
				driver.findElement(By.cssSelector("input.button:nth-child(4)")).click();
				
				driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		
				
				driver.findElement(By.id("job_preview_submit_button")).click();
				
				driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
				String submitjob = driver.findElement(By.cssSelector(".entry-content")).getText();
				Reporter.log("7. New job post result: " +submitjob);
				WebElement jobdashboard=driver.findElement(By.linkText("Job Dashboard"));
				jobdashboard.click();
				
				driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
				String jobpost = driver.findElement(By.cssSelector(".job-manager-jobs > tbody:nth-child(2) > tr:nth-child(1) > td:nth-child(1)")).getText();
				Reporter.log("  New job posted for: " +jobpost);
				
				
}		
			
			// 11. Searching for jobs and applying to them using XPath
			
			@Test (priority= 11)
			public void jobapply_11() throws InterruptedException {
				
				
				driver.findElement(By.id("menu-item-24")).click();
				driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
				driver.findElement(By.xpath("//*[@id=\"search_keywords\"]")).clear();
				driver.findElement(By.xpath("//*[@id=\"search_keywords\"]")).sendKeys("Senior Tester");
				
				driver.findElement(By.xpath("//*[@id=\"job_type_freelance\"]")).click(); 
				driver.findElement(By.xpath("//*[@id=\"job_type_internship\"]")).click();
				driver.findElement(By.xpath("//*[@id=\"job_type_part-time\"]")).click();
				driver.findElement(By.xpath("//*[@id=\"job_type_temporary\"]")).click();
			        
				
				driver.findElement(By.xpath("/html/body/div[1]/div/div/div/main/article/div/div/form/div[1]/div[4]/input")).click();
				Thread.sleep(1000);
				WebElement firstSuggest = driver.findElement(By.xpath("/html/body/div[1]/div/div/div/main/article/div/div/ul/li[1]/a/div[1]/h3"));
				//Wait for the button to be clicked
				wait.until(ExpectedConditions.elementToBeClickable(firstSuggest));
				firstSuggest.click();
				//Wait for results to load
				wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//*[@id=\"post-210\"]")));
				WebElement applyButton = driver.findElement(By.xpath("/html/body/div/div/div/div/main/article/div/div/div/div[3]/input"));
				//Wait for the button to be clicked
				wait.until(ExpectedConditions.elementToBeClickable(applyButton));
				applyButton.click();
				String email = driver.findElement(By.className("job_application_email")).getText();
				Reporter.log(" 11. Email id to apply for job is: " + email);
				
				
			}
			
			// 13. Post a job using details from an external CSV/Excel file
			
			@Test (priority =13)
			public void post_job_csv() throws IOException, CsvException {
				
				driver.findElement(By.id("menu-item-26")).click();
				driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
				driver.findElement(By.cssSelector("a.button")).click();
				//Load CSV file
		        CSVReader reader = new CSVReader(new FileReader("src/Resources/Post_job.csv"));

		        //Load content into list
		        List<String[]> list = reader.readAll();
		        System.out.println("Total number of rows are: " + list.size());

		        //Create Iterator reference
		        Iterator<String[]> itr = list.iterator();

		        //Iterate all values
		        while(itr.hasNext()) {
		            String[] str = itr.next();
		           
					String email = str[0];
					String job_title=  str[1];
					String location=  str[2];
					String Comments=  str[3];
					String application = str[4];
					String company =str [5];
					String website = str [6];
					String tagline = str [7];
					String video = str [8];
					String tweet = str [9];
					
					driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
					driver.findElement(By.id("create_account_email")).sendKeys(email);
					driver.findElement(By.id("job_title")).sendKeys(job_title);
					driver.findElement(By.id("job_location")).sendKeys(location);
					
					 Select dropdown = new Select(driver.findElement(By.id("job_type")));

				    //Select second option by visible text
				    dropdown.selectByVisibleText("Full Time");
				    
				    driver.switchTo().frame(0);
				    				
				    driver.findElement(By.cssSelector("#tinymce")).sendKeys(Comments);
					
				    driver.switchTo().parentFrame();
				    
				    driver.findElement(By.id("application")).sendKeys(application);
					driver.findElement(By.id("company_name")).sendKeys(company);
					driver.findElement(By.id("company_website")).sendKeys(website);
					driver.findElement(By.id("company_tagline")).sendKeys(tagline);
					driver.findElement(By.id("company_video")).sendKeys(video);
					driver.findElement(By.id("company_twitter")).sendKeys(tweet);
					driver.findElement(By.cssSelector("input.button:nth-child(4)")).click();
					
					driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			
					
					driver.findElement(By.id("job_preview_submit_button")).click();
					
					driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
					String submitjob = driver.findElement(By.cssSelector(".entry-content")).getText();
					System.out.println("New job post result: " +submitjob);
					WebElement jobdashboard=driver.findElement(By.linkText("Job Dashboard"));
					jobdashboard.click();
					
					driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
					String jobpost = driver.findElement(By.cssSelector(".job-manager-jobs > tbody:nth-child(2) > tr:nth-child(1) > td:nth-child(1)")).getText();
					Reporter.log("13. New job is posted using csv file and post is : " +jobpost);

		        }reader.close();
				
				
			}
			
			
	
	@AfterTest(alwaysRun = true)
    public void afterClass() {
        //Close browser
        driver.close();
    }

	
	
	
	
}
