package activity12;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Activity12_3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		WebDriver driver = new FirefoxDriver();
		WebDriverWait wait = new WebDriverWait(driver, 10);
	    Actions builder = new Actions(driver);
		
		driver.get("https://www.training-support.net/selenium/popups");
		String title = driver.getTitle();
		System.out.println("Title of page is: " + title);
		
		WebElement button = driver.findElement(By.xpath("/html/body/div[2]/div/div/div[2]/div[2]/button"));
		 builder.moveToElement(button).pause(Duration.ofSeconds(1)).build().perform();
	        String tooltipText = button.getAttribute("data-tooltip");
	        System.out.println("Tooltip text: " + tooltipText);
		
	        button.click();
	        
	        //Wait for modal to appear
	        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("signInModal")));
	        
	        //Find user name and password and fill in the details
	        driver.findElement(By.id("username")).sendKeys("admin");
	        driver.findElement(By.id("password")).sendKeys("password");
	        driver.findElement(By.xpath("//button[text()='Log in']")).click();
	        
	        //Read the login message
	        String message = driver.findElement(By.id("action-confirmation")).getText();
	        System.out.println(message);
	        
	        //Close browser
	        driver.close();
		
	}

}
