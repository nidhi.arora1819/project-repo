package activity12;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;


public class Activity12_1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		WebDriver driver = new FirefoxDriver();
		driver.get("https://www.training-support.net/selenium/iframes");
		String title = driver.getTitle();
		System.out.println("Title of page is: " + title);
		
		driver.switchTo().frame(0);
		WebElement firstHeading = driver.findElement(By.cssSelector(".sizer > div:nth-child(1) > div:nth-child(1)"));
		System.out.println("First iFrame heading is: " + firstHeading.getText());
		
		WebElement button1 = driver.findElement(By.cssSelector("#actionButton"));
		System.out.println(button1.getText());
		System.out.println(button1.getCssValue("color"));
		button1.click();
		
		System.out.println(button1.getText());
		System.out.println(button1.getCssValue("color"));
		
		driver.switchTo().defaultContent();
		driver.switchTo().frame(1);
		WebElement Heading2 = driver.findElement(By.cssSelector(".sizer > div:nth-child(1) > div:nth-child(1)"));
		System.out.println("Second iFrame heading is: " + Heading2.getText());
		
		WebElement button2 = driver.findElement(By.cssSelector("#actionButton"));
		System.out.println(button2.getText());
		System.out.println(button2.getCssValue("background-color"));
		button2.click();
		
		System.out.println(button2.getText());
		System.out.println(button2.getCssValue("background-color"));
		
		driver.switchTo().defaultContent();
		
		driver.close();
		
		
	
		
	}

}
