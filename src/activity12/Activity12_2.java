package activity12;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Activity12_2 {

	public static void main(String[] args) {
		WebDriver driver = new FirefoxDriver();
		driver.get("https://www.training-support.net/selenium/nested-iframes");
		String title = driver.getTitle();
		System.out.println("Title of page is: " + title);
		
		driver.switchTo().frame(0);
		driver.switchTo().frame(0);
		
		WebElement firstHeading = driver.findElement(By.cssSelector(".sizer > div:nth-child(1) > div:nth-child(1)"));
		System.out.println("First iFrame heading is: " + firstHeading.getText());
		
		driver.switchTo().defaultContent();
		driver.switchTo().frame(0);
		driver.switchTo().frame(1);
		WebElement Heading2 = driver.findElement(By.xpath("/html/body/div[1]/div/div/div"));
		System.out.println("Second iFrame heading is: " + Heading2.getText());
		
		driver.close();

	}

}
