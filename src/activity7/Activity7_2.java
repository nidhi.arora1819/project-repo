package activity7;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class Activity7_2 {

	public static void main(String[] args) {
		WebDriver driver = new FirefoxDriver();
		driver.get("https://training-support.net/selenium/selects");
		WebElement chosen = driver.findElement(By.id("multi-select"));
		
		Select multilist = new Select (driver.findElement(By.id("multi-value")));
		
		if(multilist.isMultiple())
		{
			multilist.selectByVisibleText("Javascript");
			System.out.println(chosen.getText());
			
			multilist.selectByValue("Node");
			System.out.println(chosen.getText());
			 //Select 4,5, and 6 options by index
            for(int i=4; i<=6; i++) {
                multilist.selectByIndex(i);
            }
            System.out.println(chosen.getText());
    
            //Deselect 'NodeJS' by value
            multilist.deselectByValue("node");
            System.out.println(chosen.getText());
            
            //Deselect 7th option by index
            multilist.deselectByIndex(7);
            System.out.println(chosen.getText());
            
            //Get all selected options
            List<WebElement> selectedOptions = multilist.getAllSelectedOptions();
            //Print all selected options
            for(WebElement selectedOption : selectedOptions) {
                System.out.println("Selected option: " + selectedOption.getText());
            }
            
            //Deselect all options
            multilist.deselectAll();
            System.out.println(chosen.getText());
        }
		driver.close();
		
			
			
	}

}
