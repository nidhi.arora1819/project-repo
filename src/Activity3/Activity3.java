package Activity3;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Activity3 {

	public static void main(String[] args) throws InterruptedException {
		WebDriver driver = new FirefoxDriver();
		driver.get("https://training-support.net/selenium/simple-form");
		String title = driver.getTitle();
		System.out.println("The title of page is: " +title);
		driver.findElement(By.id("firstName")).sendKeys("Nidhi");
		driver.findElement(By.id("lastName")).sendKeys("Arora");
		driver.findElement(By.id("email")).sendKeys("niarora4@in.ibm.com");
		driver.findElement(By.id("number")).sendKeys("123456789");
		driver.findElement(By.cssSelector(".ui.green.button")).click();
		Thread.sleep(1000);
		driver.close();
		
	}

}
