package Activity4;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class activity4_1 {

	public static void main(String[] args) {
		WebDriver driver = new FirefoxDriver();
		driver.get("https://training-support.net");
		String title = driver.getTitle();
		System.out.println("The title of page is: " +title);
		driver.findElement(By.xpath("/html/body/div/div/div/a")).click();
		System.out.println("The title of new page is: " +driver.getTitle());
		driver.close();	
		

	}

}
