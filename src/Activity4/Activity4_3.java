package Activity4;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Activity4_3 {

	public static void main(String[] args) {
		WebDriver driver = new FirefoxDriver();
		driver.get("https://www.training-support.net/selenium/target-practice");
		String title = driver.getTitle();
		System.out.println("The title of page is: " + title);
		String thirdheader = driver.findElement(By.xpath("//h3[@id = 'third-header']")).getText();
		System.out.println("Third Header is: " + thirdheader);
		String fifthheader = driver.findElement(By.xpath("//h5")).getCssValue("color");
		System.out.println("Color of fifth Header is: " + fifthheader);
		String vbuttonclass = driver.findElement(By.xpath("//button[contains(text(), 'Violet')]")).getAttribute("class");
		System.out.println("Violet button class is: " + vbuttonclass);
		String greyButton = driver.findElement(By.xpath("/html/body/div/div[2]/div/div[2]/div/div/div/div[2]/div[3]/button[2]")).getText();
        System.out.println("The grey button's text is: " + greyButton);

        //Close the browser
        driver.close();

		
	}

}
