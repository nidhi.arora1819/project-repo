package activity10;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

public class Activity10_2 {

	public static void main(String[] args) {
		WebDriver driver = new FirefoxDriver();
		Actions actions = new Actions(driver);
		driver.get("https://www.training-support.net/selenium/input-events");
		String title = driver.getTitle();
		System.out.println("Title of page is: " + title);
		
		WebElement pressedkey = driver.findElement(By.id("keyPressed"));
		
		Action actionseq1 = actions.sendKeys("N").build();
		actionseq1.perform();
		String pressedkeyText = pressedkey.getText();
		System.out.println("Pressed key is: " + pressedkeyText);
		
		 Action actionSequence2 = actions
	                .keyDown(Keys.CONTROL)
	                .sendKeys("a")
	                .sendKeys("c")
	                .keyUp(Keys.CONTROL)
	                .build();
	        actionSequence2.perform();
	        pressedkeyText = pressedkey.getText();
	        System.out.println("Pressed key is: " + pressedkeyText);
		
        //Close browser
        driver.close();

	}

}
