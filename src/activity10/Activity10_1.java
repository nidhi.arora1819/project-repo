package activity10;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;

public class Activity10_1 {

	public static void main(String[] args) {
		WebDriver driver = new FirefoxDriver();
		Actions actions = new Actions(driver);
		driver.get("https://www.training-support.net/selenium/input-events");
		String title = driver.getTitle();
		System.out.println("Title of page is: " + title);
		
		WebElement cube = driver.findElement(By.id("wrapD3Cube"));
		
		actions.click(cube);
		WebElement cubeval = driver.findElement(By.className("active"));
		System.out.println("Left click: " + cubeval.getText());
		
		actions.doubleClick(cube).perform();
        cubeval = driver.findElement(By.className("active"));
        System.out.println("Double Click: " + cubeval.getText());
        
        //Right click        
        actions.contextClick(cube).perform();
        cubeval = driver.findElement(By.className("active"));
        System.out.println("Right Click: " + cubeval.getText());

        //Close browser
        driver.close();

	}

}
